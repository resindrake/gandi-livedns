
#!/bin/bash

#Your API key
export APIKEY=""

#The subdomains (e.g. www), separated by semicolons
export RECORD_LIST=""

#The domain you wish to update
export DOMAIN=""



#The TTL for records
export TTL=1800

#Enable IPv4 and IPv6
export SET_IPV4=yes
export SET_IPV6=no
