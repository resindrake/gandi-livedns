# Gandi LiveDNS

This is a simple bash program that updates your DNS records with Gandi to the IP address of the computer the script was run on.

## Installation
1. Clone the repository to somewhere sensible
2. **Make sure** to fill out `APIKEY`, `RECORD_LIST` and `DOMAIN` in `config.sh`.
3. You can run `run.sh` by itself, or have it run periodically with cron/systemd.

## Variables
* `APIKEY` - Your Gandi API key
* `RECORD_LIST` - The subdomains (e.g. www), separated by semicolons
* `DOMAIN` - The domain you wish to update (e.g. example.com)
* `TTL` - The TTL for records (e.g. 1800)
* `SET_IPV4` (yes/no) - Whether to update the *A* record
* `SET_IPV6` (yes/no) - Whether to update the *AAAA* record

## Running Minutely
* Cron:  
    * `crontab -e`
    * Add the entry `* * * * * /path/to/run.sh`
* Systemd:
    * Edit `./systemd/gandi-livedns.service` to contain the correct working directory and file path
    * `cp ./systemd/* /etc/systemd/system/; sudo systemctl enable gandi-livedns.timer; sudo systemctl start gandi-livedns.timer`
